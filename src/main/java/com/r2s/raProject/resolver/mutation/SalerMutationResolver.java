package com.r2s.raProject.resolver.mutation;

import com.r2s.raProject.data.dto.SalerDTO;
import com.r2s.raProject.data.entity.Saler;
import com.r2s.raProject.data.mapper.SalerMapper;
import com.r2s.raProject.data.repository.AuthorRepository;
import com.r2s.raProject.data.repository.SalerRepository;
import com.r2s.raProject.exception.ResourceNotFoundException;
import graphql.kickstart.tools.GraphQLMutationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class SalerMutationResolver implements GraphQLMutationResolver {

    @Autowired
    private SalerRepository salerRepository;
    @Autowired
    private SalerMapper salerMapper;
    @Autowired
    private AuthorRepository authorRepository;


    public Saler createSaler(SalerDTO salerDTO) {
        Saler saler = salerMapper.toEntity(salerDTO);
        salerRepository.save(saler);
        return saler;
    }

    public Saler updateSaler(SalerDTO salerDTO, Long id){
        Saler saler = salerRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException(Collections.singletonMap("salerDTO", id))
        );
        salerRepository.save(saler);
        return saler;
    }

    public Boolean deleteSaler(Long id){
        salerRepository.deleteById(id);
        return true;
    }

}
