package com.r2s.raProject.resolver.mutation;

import com.r2s.raProject.common.JwtUtils;
import com.r2s.raProject.common.enumeration.ERole;
import com.r2s.raProject.data.dto.LoginRequestDTO;
import com.r2s.raProject.data.dto.user.UserCreationDTO;
import com.r2s.raProject.data.dto.user.UserDTO;
import com.r2s.raProject.data.entity.User;
import com.r2s.raProject.data.mapper.RoleMapper;
import com.r2s.raProject.data.mapper.UserMapper;
import com.r2s.raProject.data.repository.UserRepository;
import com.r2s.raProject.exception.ConflictException;
import com.r2s.raProject.exception.ResourceNotFoundException;
import com.r2s.raProject.exception.ValidationException;
import com.r2s.raProject.resolver.query.RoleQueryResolver;
import com.r2s.raProject.resolver.service.UserService;
import com.r2s.raProject.resolver.service.impl.UserDetailsImpl;
import graphql.kickstart.tools.GraphQLMutationResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Component
@Service("userDetailsService")
public class UserMutationResolver implements GraphQLMutationResolver, UserService {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private RoleQueryResolver roleQueryResolver;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public UserDTO findById(long id) {
        return null;
    }

    @Override
    public void activeUser(String token) {

    }

    @Override
    public void updateTokenForgetPassword(String email, String token) {

    }

    @Override
    public boolean checkValidOldPassword(String oldPass, String newPass) {
        return false;
    }

    @Override
    public boolean existsByEmail(String email) {
        return false;
    }

    /**
     * method find User by email
     * @param email
     * @return User
     * @author Nguyen Tien Dat
     */
    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(
                () -> new ResourceNotFoundException(Collections.singletonMap("Email ", email))
        );
    }

    @Override
    public void updateTokenActive(String email, String token) {

    }

    @Override
    public Long getCurrentUserId() {
        return null;
    }

    /**
     * Method create user
     *
     * @param userCreationDTO
     * @return show User if success
     * @author Nguyen Tien Dat
     */
    @Override
    public User createUser(UserCreationDTO userCreationDTO) {

        // check existing user info
        Map<String, Object> errors = new HashMap<String, Object>();
        if (userRepository.existsByEmail(userCreationDTO.getEmail())) {
            errors.put("email", userCreationDTO.getEmail());
        }
        if (errors.size() > 0) {
            throw new ConflictException(Collections.singletonMap("userCreationDTO", errors));
        }

        // set info for user
        User user = userMapper.toEntity(userCreationDTO);

        // set default role and status will be set default
        // base on auth provider register
        ERole defaultRole;

        user.setPassword(passwordEncoder.encode(userCreationDTO.getPassword()));
        user.setRole(roleQueryResolver.findByName(ERole.Admin.toString()));

        User newUser = userRepository.save(user);

        return newUser;
    }

//    @Override
//    public UserDTO showUserDetail() {
//        return null;
//    }
//
//    @Override
//    public UserDTO disable(long id) {
//        return null;
//    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        if (email.isEmpty()) {
            throw new ValidationException(Collections.singletonMap("User email ", email));
        }
        return UserDetailsImpl.build(findByEmail(email));
    }

//    public void updateAfterLoginOAuth(User user, OAuth2UserInfo oAuth2UserInfo, EAuthenticationProvider provider) {
//
//        if (oAuth2UserInfo.getClientName().equals("Google")) {
//
//            if (user.getFullName().isEmpty() || user.getFullName().equalsIgnoreCase(oAuth2UserInfo.getFullName()))
//                user.setFullName(oAuth2UserInfo.getFullName());
////            if (user.getAvatar() == null || user.getAvatar().isEmpty())
////                user.setAvatar(oAuth2UserInfo.getAvatar());
//            user.setAuthProvider(String.valueOf(provider));
//            user.setCreatedDate(new Date());
//            userRepository.save(user);
//        }
//    }
//    public User createAfterLoginOAuth(OAuth2UserInfo oAuth2UserInfo) {
//        UUID uuid = UUID.randomUUID();
//        User user = new User();
//        String provider = oAuth2UserInfo.getClientName();
//        if (existsByEmail(oAuth2UserInfo.getEmail()))
//            throw new ConflictException(Collections.singletonMap(oAuth2UserInfo.getEmail(), "email"));
//
//        // create with google
//        if (provider.toUpperCase().equals(EAuthenticationProvider.GOOGLE.toString())) {
//            user.setEmail(oAuth2UserInfo.getEmail());
//             user.setFullName(oAuth2UserInfo.getFullName());
//            user.setPassword(
//                    new BCryptPasswordEncoder(BCryptPasswordEncoder.BCryptVersion.$2A, 10)
//                            .encode(String.valueOf(uuid)));
//            user.setCreatedDate(new Date());
//            user.setAuthProvider(String.valueOf(provider));
//            user.setRole(roleMapper.toEntity(roleMutationResolver.findByName("Role_Author")));
//
//            return user;
//            // create with facebook
//        } else {
//            user.setEmail(oAuth2UserInfo.getEmail());
//            user.setFullName(oAuth2UserInfo.getName());
//            user.setPassword(
//                    new BCryptPasswordEncoder(BCryptPasswordEncoder.BCryptVersion.$2A, 10)
//                            .encode(String.valueOf(uuid)));
//            user.setCreatedDate(new Date());
//            user.setAuthProvider(provider);
//            user.setRole(roleMapper.toEntity(roleMutationResolver.findByName("Role_Author")));
//
//            return user;
//        }
//    }
//
//    public boolean existsByEmail(String email) {
//        return userRepository.existsByEmail(email);
//    }
}
