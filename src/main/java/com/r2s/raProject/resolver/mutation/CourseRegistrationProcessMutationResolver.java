package com.r2s.raProject.resolver.mutation;

import com.r2s.raProject.data.dto.CourseRegistrationProcessDTO;
import com.r2s.raProject.data.entity.CourseRegistrationProcess;
import com.r2s.raProject.data.entity.Method;
import com.r2s.raProject.data.mapper.CourseRegistrationProcessMapper;
import com.r2s.raProject.data.repository.CourseRegistrationProcessRepository;
import com.r2s.raProject.data.repository.MethodRepository;
import com.r2s.raProject.exception.ConflictException;
import com.r2s.raProject.exception.ResourceNotFoundException;
import graphql.kickstart.tools.GraphQLMutationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class CourseRegistrationProcessMutationResolver implements GraphQLMutationResolver {
    @Autowired
    CourseRegistrationProcessRepository courseRegistrationProcessRepository;
    @Autowired
    CourseRegistrationProcessMapper courseRegistrationProcessMapper;
    @Autowired
    MethodRepository methodRepository;

    /**
     * method create CourseRegistrationProcess
     * @author lyhai
     * @param courseRegistrationProcessDTO
     * @return created CourseRegistrationProcess
     */
    public Boolean createCourseRegistrationProcess(
            CourseRegistrationProcessDTO courseRegistrationProcessDTO) {

        Long methodId = courseRegistrationProcessDTO.getMethodId();

        Method method = methodRepository.findById(methodId)
                .orElseThrow(() -> new ResourceNotFoundException(Collections.singletonMap("methodId", methodId)));


        CourseRegistrationProcess courseRegistrationProcess = courseRegistrationProcessMapper.
                toEntity(courseRegistrationProcessDTO);

        courseRegistrationProcess.setMethod(method);

        courseRegistrationProcessRepository.save(courseRegistrationProcess);

        return true;
    }
}
