package com.r2s.raProject.resolver.mutation;

import com.r2s.raProject.data.dto.MethodDTO;
import com.r2s.raProject.data.entity.Method;
import com.r2s.raProject.data.mapper.MethodMapper;
import com.r2s.raProject.data.repository.MethodRepository;
import graphql.kickstart.tools.GraphQLMutationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MethodMutationResolver implements GraphQLMutationResolver {
    @Autowired
    MethodRepository methodRepository;
    @Autowired
    MethodMapper methodMapper;

    public Method createMethod(MethodDTO methodDTO){
        return methodRepository.save(methodMapper.toEntity(methodDTO));
    }

//    public Boolean deleteMethod(long id){
//        methodRepository.deleteById(id);
//        return true;
//    }
}
