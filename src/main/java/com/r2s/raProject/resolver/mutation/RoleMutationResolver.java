package com.r2s.raProject.resolver.mutation;

import com.r2s.raProject.common.enumeration.ERole;
import com.r2s.raProject.data.entity.Role;
import com.r2s.raProject.data.repository.RoleRepository;
import graphql.kickstart.tools.GraphQLMutationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class RoleMutationResolver implements GraphQLMutationResolver {

    @Autowired
    private RoleRepository roleRepository;

    /**
     * This is a method to create Role
     * @param name
     * @return Role
     * @author Nguyen Tien Dat
     */
    @Transactional
    public Role createRole(String name) {
        Role role = new Role();
        role.setName(name);
        return roleRepository.save(role);
    }

    /**
     * This is a method to delete Role by Id Role
     * @param id
     * @return boolean
     * @author Nguyen Tien Dat
     */
    @Transactional
    public boolean deleteRole(Integer id) {
        roleRepository.deleteById(id);
        return true;
    }

//    @PostConstruct
//    public void init() {
//        List<Role> rolesToSave = new ArrayList<>();
//        for (ERole eRole : ERole.values()) {
//            if (!roleRepository.existsByName(eRole.toString())) {
//                Role role = new Role();
//                role.setName(eRole.toString());
//                rolesToSave.add(role);
//            }
//        }
//
//        if (!rolesToSave.isEmpty()) {
//            roleRepository.saveAll(rolesToSave);
//        }
//    }
}
