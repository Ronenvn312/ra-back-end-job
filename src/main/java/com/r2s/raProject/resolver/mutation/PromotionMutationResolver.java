package com.r2s.raProject.resolver.mutation;

import graphql.kickstart.tools.GraphQLMutationResolver;
import com.r2s.raProject.data.dto.PromotionDTO;
import com.r2s.raProject.data.entity.Promotion;
import com.r2s.raProject.data.mapper.PromotionMapper;
import com.r2s.raProject.data.repository.PromotionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PromotionMutationResolver implements GraphQLMutationResolver {
    @Autowired
    private PromotionRepository promotionRepository;
    @Autowired
    private PromotionMapper promotionMapper;

    /**
     * Method create promotion
     *
     * @param promotionDTO
     * @return Returns created promotion
     * @Author AnhTuan
     */
    public Promotion createPromotion(PromotionDTO promotionDTO){
        Promotion promotion = promotionMapper.toEntity(promotionDTO);

        promotionRepository.save(promotion);

        return promotion;
    }

}
