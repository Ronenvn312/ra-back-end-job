package com.r2s.raProject.resolver.query;

import com.r2s.raProject.common.JwtUtils;
import com.r2s.raProject.common.util.oauth.CustomAuthenticationToken;
import com.r2s.raProject.data.dto.LoginRequestDTO;
import com.r2s.raProject.data.dto.user.UserShowDTO;
import com.r2s.raProject.data.entity.User;
import com.r2s.raProject.data.mapper.UserMapper;
import com.r2s.raProject.data.repository.UserRepository;
import com.r2s.raProject.exception.ResourceNotFoundException;
import com.r2s.raProject.resolver.service.UserService;
import com.r2s.raProject.resolver.service.impl.UserDetailsImpl;
import graphql.kickstart.tools.GraphQLQueryResolver;
import graphql.relay.DefaultConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserQueryResolver implements GraphQLQueryResolver {


    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserMapper userMapper;

    public final Logger LOGGER = LoggerFactory.getLogger("info");
    @Value("${url.redirect.path}")
    private String urlOAuth2;
    @Value("${domain.path}")
    private String myAppUrl;

    @Transactional
    public UserShowDTO authenticateUserGoogle(LoginRequestDTO loginRequestDTO) {
        Authentication authentication;
        // login with Google by authProvider
        if ("google".equalsIgnoreCase(loginRequestDTO.getAuthProvider())) {
            CustomAuthenticationToken authenticationToken = new CustomAuthenticationToken(loginRequestDTO.getEmail(), loginRequestDTO.getAuthProvider());
            authentication = authenticationManager.authenticate(authenticationToken);

        }else{
            authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequestDTO.getEmail(),
                            loginRequestDTO.getPassword()));
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = this.jwtUtils.generateJwtToken(authentication);
        UserDetailsImpl user = (UserDetailsImpl) authentication.getPrincipal();
        User userDto = userService.findByEmail(loginRequestDTO.getEmail());

        long id = userDto.getId();
        List<String> roles = user.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        String role = roles.isEmpty() ? null : roles.get(0);
        LOGGER.info("%s has successfully logged in.", user.getEmail());
        System.out.println(jwt);
        UserShowDTO userShowDTO = userMapper.toUserShowDTO(userDto);
        userShowDTO.setToken(jwt);
//        userDto.setTokenActive(jwt);
        return userShowDTO;
    }
}
