package com.r2s.raProject.resolver.query;

import com.r2s.raProject.data.entity.Promotion;
import com.r2s.raProject.data.mapper.PromotionMapper;
import com.r2s.raProject.data.repository.PromotionRepository;
import com.r2s.raProject.util.CursorUtil;
import graphql.kickstart.tools.GraphQLQueryResolver;
import graphql.relay.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import graphql.relay.DefaultConnection;
import graphql.relay.DefaultEdge;
import graphql.relay.DefaultPageInfo;
import graphql.relay.Edge;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PromotionQueryResolver implements GraphQLQueryResolver {

    @Autowired
    private PromotionRepository promotionRepository;
    @Autowired
    private PromotionMapper promotionMapper;
    @Autowired
    private CursorUtil cursorUtil;


    /**
     * Method show all promotions
     *
     * @param pageNumber, pageSize
     * @return Returns promotion connection
     * @Author AnhTuan
     */
    public Connection<Promotion> allPromotions(int pageNumber, int pageSize){
        List<Promotion> promotions = promotionRepository.findAll(
                PageRequest.of(pageNumber - 1, pageSize)).getContent();

        List<Edge<Promotion>> promotionEdges = promotions.stream()
                .map(promotion -> new DefaultEdge<>(
                        promotion, cursorUtil.createCursorWith(promotion.getId())))
                .collect(Collectors.toList());

        boolean hasPreviousPage = pageNumber > 1;
        boolean hasNextPage = promotions.size() == pageSize;

        var pageInfo = new DefaultPageInfo(
                cursorUtil.getFirstCursorFrom(promotionEdges),
                cursorUtil.getLastCursorFrom(promotionEdges),
                hasPreviousPage,
                hasNextPage);

        return new DefaultConnection<>(promotionEdges, pageInfo);

    }

}
