package com.r2s.raProject.resolver.query;

import graphql.kickstart.tools.GraphQLQueryResolver;
import com.r2s.raProject.data.entity.Tutorial;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.r2s.raProject.data.repository.TutorialRepository;

@Component
public class TutorialQueryResolver implements GraphQLQueryResolver {

    @Autowired
    private TutorialRepository tutorialRepository;

    public Iterable<Tutorial> findAllTutorials() {
        return tutorialRepository.findAll();
    }

    public long countTutorials() {
        return tutorialRepository.count();
    }

    public Tutorial findOneTutorial(Integer id){
        return tutorialRepository.findById(id).get();
    }
}
