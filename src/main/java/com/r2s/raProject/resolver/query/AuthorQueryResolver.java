package com.r2s.raProject.resolver.query;

import com.r2s.raProject.data.entity.Author;
import graphql.kickstart.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.r2s.raProject.data.repository.AuthorRepository;

@Component
public class AuthorQueryResolver implements GraphQLQueryResolver {


    @Autowired
    private AuthorRepository authorRepository;

    public Iterable<Author> findAllAuthors() {
        return authorRepository.findAll();
    }

    public long countAuthors() {
        return authorRepository.count();
    }
}
