package com.r2s.raProject.resolver.query;

import com.r2s.raProject.data.entity.Role;
import com.r2s.raProject.data.repository.RoleRepository;
import com.r2s.raProject.exception.ResourceNotFoundException;
import graphql.kickstart.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Component
public class RoleQueryResolver implements GraphQLQueryResolver {

    @Autowired
    private RoleRepository roleRepository;

    @Transactional
    public Iterable<Role> findAllRoles() {

        return roleRepository.findAll();
    }
    @Transactional
    public Role findByName(String name) {

        return roleRepository.findByName(name).orElseThrow(
                () -> new ResourceNotFoundException(Collections.singletonMap("Role name", name))
        );
    }
}
