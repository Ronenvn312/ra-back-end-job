package com.r2s.raProject.resolver.query;

import com.r2s.raProject.data.dto.pagination.CourseRegistrationProcessPaginationDTO;
import com.r2s.raProject.data.entity.CourseRegistrationProcess;
import com.r2s.raProject.data.repository.CourseRegistrationProcessRepository;
import graphql.kickstart.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

@Component
public class CourseRegistrationProcessQueryResolver implements GraphQLQueryResolver {

    @Autowired
    private CourseRegistrationProcessRepository courseRegistrationProcessRepository;

    public CourseRegistrationProcessPaginationDTO findAllCourseRegistrationProcess(int no, int limit){
        Page<CourseRegistrationProcess> page = this.courseRegistrationProcessRepository.findAll(
                PageRequest.of(no, limit)).map(item -> item);

        return new CourseRegistrationProcessPaginationDTO(page.getContent(),page.isFirst(), page.isLast(),
                page.getTotalPages(),
                (int)page.getTotalElements(), page.getSize(),
                page.getNumber());
    }
}
