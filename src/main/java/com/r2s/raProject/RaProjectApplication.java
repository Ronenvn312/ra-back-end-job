package com.r2s.raProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class RaProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(RaProjectApplication.class, args);
	}

}
