package com.r2s.raProject.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "report")
public class Report {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "posts")
    private int post;

    @Column(name = "likes")
    private int likes;

    @Column(name = "comments")
    private int comments;

    @Column(name = "shares")
    private int shares;

    @Column(name = "inbox")
    private int inbox;

    @Column(name = "participating_groups")
    private int participatingGroups;

    @Column(name = "leads")
    private int leads;

    @Column(name = "trainee_follow")
    private int traineeFollow;

    @Column(name = "kpi_monthly")
    private int kpiMonthly;

    @Column(name = "trainee_apply")
    private int traineeApply;

    @Column(name = "reason", columnDefinition = "NVARCHAR(500)")
    private String reason;

    @Column(name = "solution", columnDefinition = "NVARCHAR(500)")
    private String solution;

    @Temporal(TemporalType.DATE)
    @Column(name = "start_date")
    private Date startDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "end_date")
    private Date endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "saler_id", nullable = false)
    private Saler saler;
}
