package com.r2s.raProject.data.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "commission_percentage")
public class CommissionPercentage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name", unique = true, columnDefinition = "NVARCHAR(100)")
    private String name;

    @Column(name = "description", nullable = false, columnDefinition = "NVARCHAR(300)")
    private String description;

    @Column(name = "percentage", nullable = false   )
    private Float percentage;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "commission_option_id", nullable = false)
    private CommissionOption commissionOption;
}
