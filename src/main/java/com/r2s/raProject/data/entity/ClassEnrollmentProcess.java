package com.r2s.raProject.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "class_enrollment_process")
public class ClassEnrollmentProcess {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "explanation", nullable = false, columnDefinition = "NVARCHAR(500)")
    private String explanation;

    @Column(name = "image_name")
    private String imageName;

    @Column(name = "note", columnDefinition = "NVARCHAR(100)")
    private String note;

    private Float duration;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "method_id", nullable = false)
    private Method method;
    
}
