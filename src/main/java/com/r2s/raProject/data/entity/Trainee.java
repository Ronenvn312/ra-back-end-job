package com.r2s.raProject.data.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "trainee")
public class Trainee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "email", unique = true, columnDefinition = "NVARCHAR(50)")
    private String email;

    @JsonIgnore
    @Column(name = "password", nullable = false, columnDefinition = "NVARCHAR(200)")
    private String password;

    @Column(name = "full_name", nullable = false, columnDefinition = "NVARCHAR(70)")
    private String fullName;

    private int gender;

    @Temporal(TemporalType.DATE)
    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "phone_number", columnDefinition = "NVARCHAR(15)")
    private String phoneNumber;

    @Column(name = "university", columnDefinition = "NVARCHAR(50)")
    private String university;

    @Column(name = "note", columnDefinition = "NVARCHAR(100)")
    private String note;

    @Column(name = "date")
    private Date date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "saler_id", nullable = false)
    private Saler saler;
}
