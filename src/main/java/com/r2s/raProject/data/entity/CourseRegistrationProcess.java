package com.r2s.raProject.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "course_registration_process")
public class CourseRegistrationProcess {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "explanation", nullable = false, columnDefinition = "NVARCHAR(500)")
    private String explanation;

    @Column(name = "learning_support_group", columnDefinition = "NVARCHAR(100)")
    private String learningSupportGroup;

    @Column(name = "note", columnDefinition = "NVARCHAR(100)")
    private String note;

    @Column(name = "footnote", columnDefinition = "NVARCHAR(300)")
    private String footnote;

    private Float duration;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "method_id", nullable = false)
    private Method method;

}
