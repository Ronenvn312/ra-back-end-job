package com.r2s.raProject.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "course")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name", unique = true, columnDefinition = "NVARCHAR(100)")
    private String name;

    @Column(name = "total_hours", nullable = false)
    private int totalHours;

    @Column(name = "class_schedule", nullable = false)
    private String classSchedule;

    @Column(name = "tuition_fee", nullable = false)
    private BigDecimal tuitionFee;

    //course_calendar
    //method_course
}
