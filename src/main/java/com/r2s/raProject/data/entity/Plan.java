package com.r2s.raProject.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "plan")
public class Plan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "kpi_monthly")
    private int kpiMonthly;

    @Column(name = "week_one")
    private int weekOne;

    @Column(name = "week_two")
    private int weekTwo;

    @Column(name = "week_three")
    private int weekThree;

    @Column(name = "week_four")
    private int weekFour;

    @Column(name = "method", nullable = false, columnDefinition = "NVARCHAR(100)")
    private String method;

    @Column(name = "note",  columnDefinition = "NVARCHAR(100)")
    private String note;    //process distinction

    @Column(name = "author", nullable = false)
    private long author; // sales manager

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "course_calendar_id", nullable = false)
    private CourseCalendar courseCalendar;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "saler_id", nullable = false)
    private Saler saler;




}
