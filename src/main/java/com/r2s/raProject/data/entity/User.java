package com.r2s.raProject.data.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "user")
public class User extends Auditable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "email", unique = true, columnDefinition = "NVARCHAR(50)")
    private String email;

    @JsonIgnore
    @Column(name = "password", nullable = false, columnDefinition = "NVARCHAR(200)")
    private String password;

    @Column(name = "full_name", nullable = false, columnDefinition = "NVARCHAR(70)")
    private String fullName;

    private int gender;

    @Temporal(TemporalType.DATE)
    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "phone_number", columnDefinition = "NVARCHAR(15)")
    private String phoneNumber;

    @JsonIgnore
    @Column(name = "password_forgot_token", columnDefinition = "NVARCHAR(50)")
    private String passwordForgotToken;

    @JsonIgnore
    @Column(name = "token_active", columnDefinition = "NVARCHAR(500)")
    private String tokenActive;

    @Column(name = "date")
    private Date date;

    @JsonIgnore
    @Column(name = "auth_provider", columnDefinition = "NVARCHAR(10)")
    private String authProvider;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_id", nullable = false)
    private Role role;

}
