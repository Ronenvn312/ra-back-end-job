package com.r2s.raProject.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "promotion")
public class Promotion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name", unique = true, columnDefinition = "NVARCHAR(100)")
    private String name;

    @Column(name = "discount_percentage")
    private Float discountPercentage;

    @Column(name = "note", columnDefinition = "NVARCHAR(100)")
    private String note;

    @Temporal(TemporalType.DATE)
    @Column(name = "application_time")
    private Date applicationTime;

}
