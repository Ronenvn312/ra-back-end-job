package com.r2s.raProject.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "commission_kpi")
public class CommissionKpi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name", unique = true, columnDefinition = "NVARCHAR(100)")
    private String name;

    @Column(name = "commission_percentage", nullable = false)
    private Float commissionPercentage;

    @Column(name = "kpi_responsibility", nullable = false)
    private int kpiResponsibility;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "commission_option_id", nullable = false)
    private CommissionOption commissionOption;
}
