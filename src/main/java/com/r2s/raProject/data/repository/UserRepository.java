package com.r2s.raProject.data.repository;

import com.r2s.raProject.data.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    boolean existsByEmail(String email);

    Optional<User> findByEmail(String email);

    Page<User> findALLByEmailLike(String username, Pageable pageable);

    boolean existsByIdNotAndEmail(long id, String email);

    Optional<User> findByPasswordForgotToken(String token);

    User findByTokenActive(String token);
//
//    @Query("SELECT COUNT(u.id) from User u"
//            + " WHERE (:from IS NULL OR :from <= u.createdDate)"
//            + " AND (:to IS NULL OR u.createdDate <= :to)")
//    Long countByCreatedDateBetween(Date from, Date to);
//
//    @Query("SELECT u.authProvider FROM User u WHERE u.email = :email")
//    String findByEmailForAuthProvider(@Param("email") String email);
//
//    @Query("SELECT " +
//            "SUM(CASE WHEN u.role.id = 1 THEN 1 ELSE 0 END) AS adminCount, " +
//            "SUM(CASE WHEN u.role.id = 2 THEN 1 ELSE 0 END) AS candidateCount, " +
//            "SUM(CASE WHEN u.role.id = 3 THEN 1 ELSE 0 END) AS hrCount, " +
//            "SUM(CASE WHEN u.role.id = 4 THEN 1 ELSE 0 END) AS partnerCount " +
//            "FROM User u")
//    List<Object[]> countUsersByRole();
//
//
//    @Query("SELECT YEAR(u.createdDate) AS year, MONTH(u.createdDate) AS month, COUNT(u) AS count " +
//            "FROM User u " +
//            "GROUP BY YEAR(u.createdDate), MONTH(u.createdDate) " +
//            "ORDER BY YEAR(u.createdDate), MONTH(u.createdDate)")
//    List<Object[]> countAccountByMonth();
//
//    @Query("SELECT u FROM User u WHERE u.role.id = :roleId")
//    Page<User> findUsersByRoleId(@Param("roleId") int roleId,Pageable pageable);
}
