package com.r2s.raProject.data.repository;

import com.r2s.raProject.data.entity.CourseRegistrationProcess;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRegistrationProcessRepository extends JpaRepository<CourseRegistrationProcess, Long> {

}
