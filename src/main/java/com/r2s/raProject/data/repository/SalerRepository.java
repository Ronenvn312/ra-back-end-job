package com.r2s.raProject.data.repository;

import com.r2s.raProject.data.entity.Saler;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SalerRepository extends JpaRepository<Saler, Long> {
}
