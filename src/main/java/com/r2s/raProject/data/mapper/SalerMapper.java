package com.r2s.raProject.data.mapper;

import com.r2s.raProject.data.dto.SalerDTO;
import com.r2s.raProject.data.entity.Saler;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface SalerMapper {
    @Mapping(source = "userDTO", target = "user")
    Saler toEntity(SalerDTO salerDTO);

    @Mapping(source = "user", target = "userDTO")
    SalerDTO toDTO(Saler saler);
}
