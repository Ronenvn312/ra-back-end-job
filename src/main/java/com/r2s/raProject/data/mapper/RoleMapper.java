package com.r2s.raProject.data.mapper;

import com.r2s.raProject.data.dto.RoleDTO;
import com.r2s.raProject.data.entity.Role;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoleMapper {


    Role toEntity(RoleDTO roleDTO);

    RoleDTO toDTO(Role role);
}
