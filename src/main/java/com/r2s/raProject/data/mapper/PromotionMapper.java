package com.r2s.raProject.data.mapper;

import com.r2s.raProject.data.dto.PromotionDTO;
import com.r2s.raProject.data.entity.Promotion;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PromotionMapper {
    Promotion toEntity(PromotionDTO promotionDTO);

    PromotionDTO toDTO(Promotion promotion);
}
