package com.r2s.raProject.data.mapper;

import com.r2s.raProject.data.dto.user.UserCreationDTO;
import com.r2s.raProject.data.dto.user.UserDTO;
import com.r2s.raProject.data.dto.user.UserProfileDTO;
import com.r2s.raProject.data.dto.user.UserShowDTO;
import com.r2s.raProject.data.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = { RoleMapper.class })
public interface UserMapper {

	@Mapping(ignore = true, target = "password")
	User toEntity(UserCreationDTO creationDTO);

	User toEntity(UserProfileDTO userProfileDTO);

	@Mapping(source = "role", target = "roleDTO")
	UserDTO toDTO(User user);

	UserShowDTO toUserShowDTO(User user);
}
