package com.r2s.raProject.data.mapper;
import com.r2s.raProject.data.dto.MethodDTO;
import com.r2s.raProject.data.entity.Method;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MethodMapper {
    Method toEntity(MethodDTO methodDTO);

    MethodDTO toDTO(Method method);

}
