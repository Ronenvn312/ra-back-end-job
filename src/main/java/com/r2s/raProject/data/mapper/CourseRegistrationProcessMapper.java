package com.r2s.raProject.data.mapper;

import com.r2s.raProject.data.dto.CourseRegistrationProcessDTO;
import com.r2s.raProject.data.entity.CourseRegistrationProcess;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = MethodMapper.class)
public interface CourseRegistrationProcessMapper {
    CourseRegistrationProcess toEntity(CourseRegistrationProcessDTO courseRegistrationProcessDTO);
    CourseRegistrationProcessDTO toDTO(CourseRegistrationProcess courseRegistrationProcess);
}
