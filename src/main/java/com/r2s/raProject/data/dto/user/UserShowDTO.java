package com.r2s.raProject.data.dto.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.r2s.raProject.data.dto.RoleDTO;
import com.r2s.raProject.data.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserShowDTO {

    private long id;
    private String email;
    private String password;
    private String fullName;

    private int gender;
    private Date birthday;
    private String phoneNumber;
    private String passwordForgotToken;
    private String tokenActive;
    private Date date;
    private String authProvider;
    private Role Role;
    private String token;
}
