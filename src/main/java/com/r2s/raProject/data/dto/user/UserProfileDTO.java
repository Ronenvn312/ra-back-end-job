package com.r2s.raProject.data.dto.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.r2s.raProject.constant.Constant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserProfileDTO {

    @Email(message = "error.emailFormat")
    @NotEmpty(message = "error.emailNotNull")
    private String email;
    private String fullName;
    private int gender;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constant.DATE_FORMAT)
    private Date birthday;
    private String phoneNumber;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constant.DATE_FORMAT)
    private Date date;

}
