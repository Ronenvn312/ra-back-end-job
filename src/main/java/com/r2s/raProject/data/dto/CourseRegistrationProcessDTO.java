package com.r2s.raProject.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigInteger;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CourseRegistrationProcessDTO implements Serializable {
    private String footnote;

    private String explanation;

    private Float duration;

    private String learningSupportGroup;

    private String note;

    private Long methodId;

}
