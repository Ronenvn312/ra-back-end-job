package com.r2s.raProject.data.dto.pagination;

import com.r2s.raProject.data.entity.CourseRegistrationProcess;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class CourseRegistrationProcessPaginationDTO {

    private List<CourseRegistrationProcess> contents;

    private boolean first;

    private boolean last;

    private int totalPages;

    private int totalItems;

    private int no;

    private int limit;
}