package com.r2s.raProject.data.dto;

import com.r2s.raProject.data.dto.user.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SalerDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private int id;
    private UserDTO userDTO;
    private BigDecimal basicSalary;
}
