package com.r2s.raProject.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PromotionDTO implements Serializable {
    private String name;

    private Float discountPercentage;

    private String note;

    private Date applicationTime;
}
