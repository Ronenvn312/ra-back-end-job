package com.r2s.raProject.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class RoleDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private int id;
    private String name;

}
