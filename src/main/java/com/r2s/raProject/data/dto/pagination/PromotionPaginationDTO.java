package com.r2s.raProject.data.dto.pagination;

import com.r2s.raProject.data.entity.Promotion;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class PromotionPaginationDTO{
    private List<Promotion> contents;

    private boolean First;

    private boolean Last;

    private int totalPages;

    private int totalItems;

    private int limit;

    private int no;

}
