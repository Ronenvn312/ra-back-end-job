package com.r2s.raProject.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LoginRequestDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String email;
	private String password;
	private String authProvider;
}
