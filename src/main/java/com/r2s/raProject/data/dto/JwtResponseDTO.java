package com.r2s.raProject.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JwtResponseDTO implements Serializable {
	private String token;
	private String type = "Bearer";
	private String email;
	private String role;
	private int gender;
	private long idUser;
	private String message;

	public JwtResponseDTO(String accessToken, String email, String roles,int gender,  long id,
                          String message) {
		this.idUser = id;
		this.token = accessToken;
		this.gender = gender;
		this.email = email;
		this.role = roles;
		this.message = message;
	}
}
