package com.r2s.raProject.common.enumeration;

public enum EAuthenticationProvider {

    GOOGLE,
    FACEBOOK
}
