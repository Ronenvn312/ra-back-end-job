package com.r2s.raProject.common.enumeration;

public enum ERole {
    Admin("Role_Admin"),
    Saler("Role_Saler");

    public static int adminRole =1 ;
    public static int salerRole = 2;


    private final String NAME;

    ERole(String NAME) {
        this.NAME = NAME;
    }

    @Override
    public String toString() {
        return NAME;
    }
}
