package com.r2s.raProject.exception;


import java.util.Map;

public class InternalServerErrorException extends ExceptionCustom {
    public InternalServerErrorException(Map<String, Object> errors) {
        super("UNEXPECTED ERROR OCCURRED", errors);
    }

}
