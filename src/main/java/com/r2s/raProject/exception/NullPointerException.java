package com.r2s.raProject.exception;

public class NullPointerException extends ExceptionCustom {
    public NullPointerException() {
        super("NULL POINTER", "NO PARAM");
    }
}
