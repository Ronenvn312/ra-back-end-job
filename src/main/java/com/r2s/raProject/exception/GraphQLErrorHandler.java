package com.r2s.raProject.exception;

import graphql.ExceptionWhileDataFetching;
import graphql.GraphQLError;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class GraphQLErrorHandler implements graphql.kickstart.execution.error.GraphQLErrorHandler {
    @Override
    public List<GraphQLError> processErrors(List<GraphQLError> errors) {
        return errors.stream()
                .map(this::getNested)
                .collect(Collectors.toList());
    }

    private GraphQLError getNested(GraphQLError e) {
        if (e instanceof ExceptionWhileDataFetching){
            ExceptionWhileDataFetching dataFetchingEx = (ExceptionWhileDataFetching) e;
            if(dataFetchingEx.getException() instanceof GraphQLError){
                return (GraphQLError) dataFetchingEx.getException();
            }
        }
        return e;
    }
}
