package com.r2s.raProject.exception;

import java.util.Map;

public class CannotDeleteException extends ExceptionCustom{
    public CannotDeleteException(Map<String, Object> errors) {
        super("CANNOT DELETE", errors);
    }
}
